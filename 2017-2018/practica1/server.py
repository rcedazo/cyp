#!/usr/bin/python3
#Servidor que queda a la espera de que se conecte algún cliente, 
#reciba dos sumandos, calcule el resultado y se lo envíe al cliente
import socket
import sys
 
def server():
	# Check number of parameters
	if len (sys.argv) != 3:
		print('Usage: python3 server.py IP PORT')
		sys.exit(1)
	host = sys.argv[1]
	port = int(sys.argv[2])
	mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	mySocket.bind((host,port))
	mySocket.listen(1)

	while True:
		conn, addr = mySocket.accept()
		print ("Connection from: " + str(addr))
		data1 = conn.recv(1024).decode()
		if not data1:
			break
		print ("Num1: " + str(data1))
		data2 = conn.recv(1024).decode()
		if not data2:
			break
		print ("Num2: " + str(data2)) 
		sum = int(data1) + int(data2)
		print ("sending sum: " + str(sum))
		conn.send(str(sum).encode())
	
	conn.close()
     
server()
