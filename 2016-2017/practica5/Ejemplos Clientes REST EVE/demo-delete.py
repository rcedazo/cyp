#!/usr/bin/python3
import http.client
import urllib.request
import urllib.parse
import json


api_url = "http://127.0.0.1:5000/medida/59108e18a67fff063bc8d6fe"

req = urllib.request.Request(api_url)

# To delete an item, it is necessary to include a header with the _etag
# Simply calling a DELETE request won't delete the item. In order to delete an item, 
# we also need to provide an _etag related to a particular item. Once item id and _etag match, 
# the item is deleted from the database.
req.add_header('If-Match','eb2f468381f0901c51b3f3528a20e80168d0a0cd')

req.get_method = lambda: "DELETE"
f = urllib.request.urlopen(req)
print(f.read().decode('utf-8'))



    
