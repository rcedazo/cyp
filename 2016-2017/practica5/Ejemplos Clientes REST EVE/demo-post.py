#!/usr/bin/python3
import http.client
import urllib.request
import urllib.parse
import json

import time


api_url = "http://127.0.0.1:5000/medida/"

headers = {'Content-type': 'application/json; charset=utf-8'}

params = {"tipo": "temp",
      "identificador": "t002",
      "ciudad": "madrid",
      "fecha": "Mon, 09 May 2016 21:29:13 GMT",
      "valor": 30}
binary_data = json.dumps(params).encode('ascii')

req = urllib.request.Request(api_url,binary_data,headers)

#req.get_method = lambda: "POST"
f = urllib.request.urlopen(req)

#print(f.read().decode('utf-8'))
data=json.loads(f.read().decode('utf-8'))
print(json.dumps(data,indent=4,sort_keys=True))
