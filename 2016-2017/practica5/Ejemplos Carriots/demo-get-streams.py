#!/usr/bin/python3
import http.client
import urllib.request
import urllib.parse
import json

import time

http.client.HTTPConnection.debuglevel = 1

api_url = "http://api.carriots.com/streams"
#api_url = "http://api.carriots.com/streams?max=2&order=-1" # To get the 2 most recent streams
api_key = "5f62b66dfbe2ae01628642018618e4397c6769c5a06171d5d2c47969e08b6bb7"  # Replace with your APIKEY
content_type = "application/json"     


# Note that sometimes you won't get a reading and
# the results will be null (because Linux can't
# guarantee the timing of calls to read the sensor).
# If this happens try again!

header = {"carriots.apikey": api_key}

req = urllib.request.Request(api_url,None,header)
req.get_method = lambda: "GET"
f = urllib.request.urlopen(req)

data=json.loads(f.read().decode('utf-8'))
print(json.dumps(data,indent=4,sort_keys=True))
