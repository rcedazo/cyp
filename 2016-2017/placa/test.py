#!/usr/bin/python3
from camera import *
from leds import *
from temperature import read_temp
from buttons import *
from reed import *
import time 

# CAMERA
takePicture()

# LEDS
all_leds_on()
time.sleep(3)
all_leds_off()
time.sleep(2)
switch_on_led(PIN_LED1)
time.sleep(2)
switch_off_led(PIN_LED1)

# TEMPERATURE
print(read_temp())

# BUTTONS
getStatesButtons()

# REED
getStateReed()
