#include <stdio.h>
#include <sys/time.h> 
#include <sys/types.h>    
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>

#define MAX_THREADS 10

void * sumar(void *arg) {
	int sc;
  	int num1, num2, res; 
  	char mensaje1[40] = "Introduce el primer operando";
  	char mensaje2[40] = "Introduce el segundo operando";

	sc = *(int *)arg;

	/* envia el mensaje pidiendo el primer numero */
    	if (write(sc, mensaje1, sizeof(mensaje1)) < 0){
      		printf("Error en write\n");
      		pthread_exit(-1);
    	}		
	
    	 /* recibe la petición, un número entero */
	 if (read(sc, (char *)&num1, sizeof(int)) < 0){
	      	printf("Error en read\n");
      		pthread_exit(-1);
	 }		

	/* envia el mensaje pidiendo el segundo numero */
    	if (write(sc, mensaje2, sizeof(mensaje2)) < 0){
      		printf("Error en write\n");
      		pthread_exit(-1);
    	}		
	
    	/* recibe la petición, un número entero */
    	if (read(sc, (char *)&num2, sizeof(int)) < 0){
     		printf("Error en read\n");
      		pthread_exit(-1);
	}

	/* los datos se transforman del formato de red al del computador */
	printf ("Sumando: %d + %d\n", ntohl(num1), ntohl(num2));
	res = ntohl(num1) + ntohl(num2); /* obtiene el resultado */
    	res = htonl(res);
        /* el resultado se transforma a formato de red */
        /* envía el resultado y cierra la conexión */
    	if (write(sc, (char *)&res, sizeof(int)) < 0){
      		printf("Error en write\n");
      		pthread_exit(-1);
    	}
    	close(sc);

	pthread_exit(0);
}


int main(void)
{
  struct sockaddr_in server_addr,client_addr;
  int sd, sc;
  int size;

  int nThreads = 0;
  pthread_attr_t attr;
  pthread_t thid[MAX_THREADS];

  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

  sd = socket(AF_INET, SOCK_STREAM, 0);
  if (sd < 0) {
    printf("Error en la llamada socket\n");
    return 1;
  }

  server_addr.sin_family = AF_INET;
  server_addr.sin_addr.s_addr = INADDR_ANY; // inet_addr("127.0.0.1");
  server_addr.sin_port = htons(4200);

  int on=1;
  setsockopt(sd,SOL_SOCKET,SO_REUSEADDR,&on,sizeof(on));

  if (bind(sd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0){
    printf("Error en la llamada bind\n");
    return 1;
  }
  listen(sd, 5);
  size = sizeof(client_addr);
  while (1){
    printf("esperando conexion\n");
    sc = accept(sd, (struct sockaddr *)&client_addr, &size);

    if (sc < 0){
      printf("Error en accpet\n");
      break;
    }
    else {
        pthread_create(&thid[nThreads], &attr, sumar, (void *)&sc);
	nThreads++;
    }
  }
  close (sd);
  return 0;
}


