cmake_minimum_required (VERSION 2.8)
project (SERVER-CLIENT)

include_directories(/usr/local/include)
link_directories(/usr/local/lib)

add_executable(server server.cpp)
target_link_libraries(server zmq)

add_executable(client client.cpp)
target_link_libraries(client zmq)

#find_library(ZMQ_LIB libzmq)
#target_link_libraries(hello ${ZMQ_LIB})














