//
//  Hello World server in C++
//  Binds REP socket to tcp://*:5555
//  Expects "Hola" from client, replies with "Que tal"
//
#include <zmq.hpp>
#include <string>
#include <iostream>



int main () {
    //  Prepare our context and socket
    zmq::context_t context (1);
    zmq::socket_t socket (context, ZMQ_REP);
    socket.bind ("tcp://*:5555");

    while (true) {
        zmq::message_t request;

        //  Wait for next request from client
        socket.recv (&request);

		std::string rpl = std::string(static_cast<char*>(request.data()), request.size());
		std::cout << "Recibido: " << rpl << std::endl;

        //  Do some 'work'
        if (rpl.compare("Hola")==0){
			//  Send reply back to client
			std::cout<<"Dentro"<<std::endl;
			zmq::message_t reply (7);
			memcpy (reply.data (), "Que tal", 7);
			socket.send (reply);
		}
        


    }
    return 0;
}

