//
//  Hello World client in C++
//  Connects REQ socket to tcp://localhost:5555
//  Sends "Hola" to server, expects message back
//
#include <zmq.hpp>
#include <string>
#include <iostream>
#include <unistd.h>

int main ()
{
    //  Prepare our context and socket
    zmq::context_t context (1);
    zmq::socket_t socket (context, ZMQ_REQ);
    socket.connect ("tcp://localhost:5555");

    //  Do 10 requests, waiting each time for a response
    for (int request_nbr = 0; request_nbr != 10; request_nbr++) {
        zmq::message_t request (4);
        memcpy (request.data (), "Hola", 4);
        std::cout << "Enviando Hola " << request_nbr << std::endl;
        socket.send (request);

        //  Get the reply.
        zmq::message_t reply;
        socket.recv (&reply);
        
        std::string rpl = std::string(static_cast<char*>(reply.data()), reply.size());
        std::cout << "Received: " << rpl << std::endl;
        sleep(1);
    }
    return 0;
}

