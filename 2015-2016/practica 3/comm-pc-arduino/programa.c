#include "serial.h"
#include <stdio.h>

int main(int argc,char **argv)
{	
	//Se abre el puerto serie
	int serial=serial_open("/dev/ttyACM0", B9600);
	printf ("Comando enviado: serial_open. Valor devuelto: %d \n", serial);	

	char mensaje[20];
	scanf("%s", mensaje);
	serial_send(serial, mensaje, 20);
	printf("Comando enviado: serial_send. Esperando respuesta\n");
	// Si es una operacion de lectura, se reciben los datos
	if (mensaje[0] == 'r') {
		char datos[20];
		int nbytes = serial_read(serial, datos, 20, 2000000);
		printf("Comando enviado: serial_read. Bytes leidos: %d\n", nbytes);
		printf("Dato leido: %s\n", datos);
	}
	printf("Operacion finalizada\n");
	serial_close(serial);
}
